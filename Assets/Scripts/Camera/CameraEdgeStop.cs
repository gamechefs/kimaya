﻿using UnityEngine;
using System.Collections;

public class CameraEdgeStop : MonoBehaviour {
	private float leftBound;
	private float rightBound;
	private float topBound;
	private float bottomBound;
	
	private Vector3 pos;
	private Transform target;
	private SpriteRenderer spriteBounds;

	private float nextTimeToSearch = 0;
	
	// Use this for initialization
	void Start () 
	{
		float vertExtent = Camera.main.camera.orthographicSize;  
		float horzExtent = vertExtent * Screen.width / Screen.height;
		
		spriteBounds = GameObject.Find("Ground").GetComponentInChildren<SpriteRenderer>();
		
		target = GameObject.FindWithTag("Player").transform;
		
		leftBound = (float)(horzExtent - spriteBounds.sprite.bounds.size.x / 2.5f);
		rightBound = 230f;
		bottomBound = (float)(vertExtent - spriteBounds.sprite.bounds.size.y / 2.0f);
		topBound = (float)(spriteBounds.sprite.bounds.size.y  / 2.0f - vertExtent);
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(target == null)
		{
			FindPlayer();
			return;
		}

		var pos = new Vector3(target.position.x, target.position.y, transform.position.z);
		pos.x = Mathf.Clamp(pos.x, leftBound, rightBound);
		pos.y = Mathf.Clamp(pos.y, bottomBound, topBound);
		transform.position = pos;

//		Debug.Log ("left"+leftBound);
//		Debug.Log ("right"+rightBound);
	}

	private void FindPlayer()
	{
		if(nextTimeToSearch <= Time.time)
		{
			GameObject serachResult = GameObject.FindGameObjectWithTag("Player");
			if(serachResult != null)
			{
				target = serachResult.transform;
			}
			nextTimeToSearch = Time.time + 0.5f;
		}
	}
	
	void OnLevelWasLoaded()
	{
		Start();
	}
}
