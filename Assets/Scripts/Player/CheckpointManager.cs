﻿using UnityEngine;
using System.Collections;

public class CheckpointManager : MonoBehaviour {

	public static CheckpointManager cpManager;
	public Transform initialSpawnPoint;

	public Transform playerPrefab;
	

	void Start()
	{
		if(cpManager == null)
		{
			cpManager = GameObject.FindGameObjectWithTag("CheckpointManager").GetComponent<CheckpointManager>();
		}
	//	Instantiate(playerPrefab,initialSpawnPoint.position,initialSpawnPoint.rotation);

	}

	public void RespawnPlayer()
	{

		Debug.Log("x: "+initialSpawnPoint.position.x);

		Instantiate(playerPrefab,initialSpawnPoint.position,initialSpawnPoint.rotation);

	}

	public static void KillPlayer(GameObject kplayer)
	{
		Destroy(kplayer);
		cpManager.RespawnPlayer();
	}

	public static void updateSpawnPoint(Transform t)
	{
		cpManager.initialSpawnPoint = t;
	}

}
