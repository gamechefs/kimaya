﻿using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;

namespace UnitySampleAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D character;
        [HideInInspector]
		public bool jump;
		private bool canHold = false;
		private bool dragOnHold = false;
		private bool climb = false;
		private bool climbing = false;
		//private int loadLevel = 1;
		public float zoomValue;
		private GameObject dragObject;
		private GameObject rollObject;
		private Spider spider;
		private RockSliding rockSound;
		private OtherSounds otherSounds;

        private void Awake()
        {
			zoomValue = 3.0f;
            character = GetComponent<PlatformerCharacter2D>();
			rockSound = GameObject.Find ("BigRock").GetComponent<RockSliding>();
			otherSounds = GetComponent<OtherSounds> ();
			dragObject = GameObject.Find("Box");
        }

        private void Update()
        {
//			Debug.Log (gameObject.transform.position);
			if(!jump && (!climb || (climb && !character.facingRight)))
			{
				// Read the jump input in Update so button presses aren't missed.
				jump = CrossPlatformInputManager.GetButtonDown("Jump");
			}
			if(!climb && climbing && CrossPlatformInputManager.GetButtonDown("Jump")){
				
				climbing = false;
				GameObject climbOnObject = GameObject.FindGameObjectWithTag("ClimbOn");
				float x = climbOnObject.transform.position.x;
				character.rigidbody2D.position = new Vector2(x,(character.rigidbody2D.position.y + 0.2f)); 
				character.rigidbody2D.isKinematic = false;
			}
			
			if(character.facingRight && climb && CrossPlatformInputManager.GetButtonDown("Jump")){
				climbing = character.Climb();
				climb = false;
			}
        }

        private void FixedUpdate()
        {
            if (!climbing) {
				// Read the inputs.
				bool crouch = CrossPlatformInputManager.GetButton ("Crouch");
				float h = CrossPlatformInputManager.GetAxis ("Horizontal");
            	
				//Check if player wants to drag an object or just move and calling the respective functions.
				if (canHold && (CrossPlatformInputManager.GetButton ("Hold")
				    || ((rigidbody2D.transform.position.x < dragObject.transform.position.x) && h>0 && !dragOnHold)
				    || ((rigidbody2D.transform.position.x > dragObject.transform.position.x) && h<0 && !dragOnHold)))
					character.DragObject (h, dragObject);

				else
					character.Move (h, crouch, jump);
            	
				jump = false;
			}
			else {
				float h = CrossPlatformInputManager.GetAxis ("Horizontal");
				if(h<0) {
					climbing = false;
					character.rigidbody2D.isKinematic = false;
					character.Move (h, false, jump);
				}
			}
        }

		void OnTriggerEnter2D(Collider2D other) 
		{
			// Check the tags of the colliders
			if (other.tag == "CamZoomIn")
				zoomValue = 2.8f;
			else if (other.tag == "CamZoomOut")
				zoomValue = 3.2f;
			else if (other.tag == "Interactive") {
				canHold = true;
					
				if (other.name == "trap_handle_l" || other.name == "trap_handle_r") {
					dragOnHold = true;
					dragObject = other.transform.parent.gameObject;
				} else
					dragObject = other.gameObject;
			} else if (other.tag == "KillZone") {
				otherSounds.DeathSound();
				AutoFade.LoadLevel (character.gameObject, 0.2f, 3f, Color.black);
			} else if (other.tag == "Climb" & !climb) {
				climb = true;
			} else if (other.tag == "RockTrig") {
				rollObject = GameObject.Find ("BigRock");
				rollObject.rigidbody2D.isKinematic = false;
				rockSound.RockRoll();
			} else if (other.tag == "SpiderAwake") {
				spider = GameObject.Find ("Spider").GetComponent<Spider>();
				spider.SpiderAwake();
			}
		}

		void OnTriggerExit2D(Collider2D other) 
		{
			// Resets the zoom value
			if (other.tag == "CamZoomIn" || other.tag == "CamZoomOut")
				zoomValue = 3.0f;
			else if (other.tag == "Interactive")
			{
				canHold = false;
				dragOnHold = false;
			}
			else if(other.tag == "Climb" && climb)
			{
				climb = false;
			}
			else if(other.tag == "Checkpoint")
			{
				CheckpointManager.updateSpawnPoint(other.transform);
			}
		}
    }
}