using UnityEngine;
using System.Collections;

public class RunSound : MonoBehaviour {
	public AudioSource leftFoot;
	public AudioSource rightFoot;
	public AudioSource[] sources;
	private float speed;
	public Animator anim;

	void Start() {
		sources = GetComponents<AudioSource> ();
		leftFoot = sources [0];
		rightFoot = sources [1];
		anim = GetComponent<Animator> ();
	}

	void Update() {
		speed = gameObject.GetComponent<Animator> ().GetFloat ("Speed");
		if (speed > 0F) {
			if (anim.GetCurrentAnimatorStateInfo (0).IsName ("Run")) {
			StartCoroutine (WaitLeft ());
			StartCoroutine (WaitRight ());
			}
		}
	}

	IEnumerator WaitLeft() {
		if (audio.isPlaying != leftFoot.clip) {
			audio.Stop ();
			leftFoot.Play ();
		}else if (speed == 0F) {
			audio.Stop();
		}
		yield return new WaitForSeconds(leftFoot.clip.length);
	}

	IEnumerator WaitRight() {
		if (audio.isPlaying != rightFoot.clip) {
			audio.Stop ();
			rightFoot.Play ();
		}else if (speed == 0F) {
			audio.Stop();
		}
		yield return new WaitForSeconds(rightFoot.clip.length);
	}
}