﻿using UnityEngine;
using System.Collections;

public class OtherSounds : MonoBehaviour {
	private RunSound runSound;
	public AudioSource source;
	public AudioClip[] clips;

	void Start() {
		runSound = gameObject.GetComponent<RunSound> ();
		runSound.sources = GetComponents<AudioSource> ();
		source = runSound.sources [2];
	}

	public void DeathSound () {
		source.clip = clips [Random.Range (0, 1)];
		source.Play ();
	}

	void Update() {
		if (runSound.anim.GetCurrentAnimatorStateInfo (0).IsName ("Crouch")) {
			runSound.leftFoot.Stop ();
			runSound.rightFoot.Stop ();
			source.clip = clips [2];
			source.Play ();
		} else if (runSound.anim.GetCurrentAnimatorStateInfo (0).IsName ("Run") || 
		           runSound.anim.GetCurrentAnimatorStateInfo (0).IsName ("Idle")) {
			source.Stop();
		}
	}
}
