﻿using UnityEngine;
using System.Collections;

public class BearTrap : MonoBehaviour {
	public Animator anim;
	private bool JumpOver;	
	private TrapSound trapSound;

	void Awake(){
		trapSound = GameObject.Find("trap").GetComponent<TrapSound> ();
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Player") {
			JumpOver = true;
			trapSound.PlaySound ();
		}
		anim.SetBool ("JumpOver", JumpOver);
	}

	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player") 
			JumpOver = false;
		anim.SetBool ("JumpOver", JumpOver);
	}
}
