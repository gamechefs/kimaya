﻿using UnityEngine;
using System.Collections;

public class RockSliding : MonoBehaviour {
	public AudioSource rollingSource;
	public AudioSource collisionSource;
	public AudioClip clip;

	void Awake () {
		AudioSource[] sources = GetComponents<AudioSource> ();
		rollingSource = sources [0];
		collisionSource = sources [1];
	}

	public void RockRoll() {
		rollingSource.Play ();
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Ground") {
			rollingSource.Stop ();
			collisionSource.Play ();
			gameObject.tag = "Item";
		}
	}
}
