﻿using UnityEngine;
using System.Collections;

public class TrapSound : MonoBehaviour {
	public AudioSource source;
	public AudioClip[] clips;

	void Awake () {
		source = GetComponent<AudioSource> ();
	}

	public void PlaySound () {
		source.clip = clips [Random.Range (0, clips.Length)];
		source.Play();
	}
}
