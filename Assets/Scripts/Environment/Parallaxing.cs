﻿using UnityEngine;
using System.Collections;

public class Parallaxing : MonoBehaviour {

	public Transform[] backgrounds; // Array of all the backgrounds to be parallaxed
	private float[] parallaxScales; // Proportion of camera movement to move the backgrounds
	public float smoothing = 1.0f; // How smooth parallax would be, should be greater than 0.

	private Transform cam; // Reference to main camera's transform
	private Vector3 previousCamPosition; // Store the position of camera in the previous frame.

	// Calls all game logic before start and after all the game objects are set up. Great for references..
	void Awake() {
		// Set up the camera reference
		cam = Camera.main.transform;
	}

	// Use this for initialization
	void Start () {
		// Previous frame has current frame's camera position.
		previousCamPosition = cam.position;

		// The length of parallax scales will be same as that of backgrounds since a parallax scale is associated
		// one background. Assigning corresponding parallax scales.
		parallaxScales = new float[backgrounds.Length];

		for (int i = 0; i < backgrounds.Length; i++) {
			parallaxScales[i] = backgrounds[i].position.z*-1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < backgrounds.Length; i++) {
			// parallax is opposite of camera movement
			float parallax = (previousCamPosition.x - cam.position.x) * parallaxScales[i];

			// The new background position's X value when parallax is added to the current position of camera. Only the 
			// X value is affected, so parallax value is added only to the X-coordinate.
			float backgroundTargetX = backgrounds[i].position.x + parallax;

			// The background position is calculated, all the three coordinates.
			Vector3 backgroundTargetPos = new Vector3(backgroundTargetX, backgrounds[i].position.y, backgrounds[i].position.z);

			// The fading between the previous background and the current background is computed using lerp.
			backgrounds[i].position = Vector3.Lerp(backgrounds[i].position, backgroundTargetPos, smoothing * Time.deltaTime);
		}	

		previousCamPosition = cam.position;
	}
}
