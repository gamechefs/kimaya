﻿using UnityEngine;
using System.Collections;

public class Spider : MonoBehaviour {
	public Animator anim;
	private bool IsAwake;

	public void SpiderAwake() {
		IsAwake = true;
		anim.SetBool ("IsAwake", IsAwake);
	}
}
